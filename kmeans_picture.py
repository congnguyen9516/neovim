import matplotlib.pyplot
import numpy
from sklearn.cluster import KMeans

img = matplotlib.pyplot.imread("tree.jpg")

width = img.shape[0]
height = img.shape[1]

img = img.reshape(width * height, 3)

kmeans = KMeans(n_clusters=6).fit(img)

labels = kmeans.predict(img)

clusters = kmeans.cluster_centers_

new_image = numpy.zeros_like(img)

for i in range(len(new_image)):
	new_image[i] = clusters[labels[i]]

new_image = new_image.reshape(width, height, 3)

matplotlib.pyplot.imshow(new_image)
matplotlib.pyplot.show()
